unit Unit3;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, lcp, Vcl.ComCtrls, Vcl.Menus;

type
  TForm3 = class(TForm)
    GroupBox2: TGroupBox;
    autostart_bool: TCheckBox;
    copy_to_clipboard_bool: TCheckBox;
    save_to_folder_bool: TCheckBox;
    folder_path: TEdit;
    select_folder_butt: TButton;
    format_img: TComboBox;
    GroupBox1: TGroupBox;
    key_save_img: THotKey;
    Button3: TButton;
    Button1: TButton;
    Label1: TLabel;
    autostart_tray_bool: TCheckBox;
    close_tray_bool: TCheckBox;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button3Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure select_folder_buttClick(Sender: TObject);
    procedure save_to_folder_boolClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure autostart_boolClick(Sender: TObject);
  private
    procedure save_folder_active();
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

{$R *.dfm}

uses Unit1, Unit2;

procedure TForm3.autostart_boolClick(Sender: TObject);
begin
autostart_tray_bool.Enabled:=autostart_bool.Checked;
end;

procedure TForm3.Button1Click(Sender: TObject);
begin
if (form1.register_hot_key(ShortCutToText(key_save_img.HotKey))=false) then
begin
  MessageBox(Application.Handle,pointer('�� ������� ���������������� ������� ������: '+ShortCutToText(key_save_img.HotKey)),pointer(application.Title), MB_OK or MB_ICONError or MB_DEFBUTTON1);
  exit();
end;
LCP_set_registry(0,'\Software\ScreenShoper','autostart_tray_bool',booltostr(autostart_tray_bool.Checked));
LCP_set_registry(0,'\Software\ScreenShoper','close_tray_bool',booltostr(close_tray_bool.Checked));
LCP_set_registry(0,'\Software\ScreenShoper','CopyToClipboard',booltostr(copy_to_clipboard_bool.Checked));
LCP_set_registry(0,'\Software\ScreenShoper','SaveToFolder',booltostr(save_to_folder_bool.Checked));
LCP_set_registry(0,'\Software\ScreenShoper','FolderPath',folder_path.Text);
LCP_set_registry(0,'\Software\ScreenShoper','key_save_img',ShortCutToText(key_save_img.HotKey));
if (autostart_bool.Checked=true) then
begin
  case autostart_tray_bool.Checked of
  true:LCP_set_registry(0,'\SOFTWARE\Microsoft\Windows\CurrentVersion\Run','ScreenShoper',application.ExeName+' /auto');
  false:LCP_set_registry(0,'\SOFTWARE\Microsoft\Windows\CurrentVersion\Run','ScreenShoper',application.ExeName);
  end;
end
else
begin
  LCP_delete_registry(0,'\SOFTWARE\Microsoft\Windows\CurrentVersion\Run','ScreenShoper');
end;
case format_img.ItemIndex of
  0:LCP_set_registry(0,'\Software\ScreenShoper','format_img','jpg');
  1:LCP_set_registry(0,'\Software\ScreenShoper','format_img','png');
  2:LCP_set_registry(0,'\Software\ScreenShoper','format_img','bmp');
else
LCP_set_registry(0,'\Software\ScreenShoper','format_img','jpg');
end;
form1.Action1.ShortCut:=key_save_img.HotKey;
close();
end;

procedure TForm3.select_folder_buttClick(Sender: TObject);
var
  Dir: string;
begin
  Dir := folder_path.Text;
  if (LCP_SelectDirectory('�������� �������',Dir,form3.Handle)) then
  begin
    folder_path.Text:=Dir;
  end;
end;

procedure TForm3.Button3Click(Sender: TObject);
begin
close;
end;

procedure TForm3.FormClose(Sender: TObject; var Action: TCloseAction);
begin
form1.Enabled:=true;
form2.Enabled:=true;
end;

procedure TForm3.FormShow(Sender: TObject);
begin
if (LCP_get_registry(0,'\SOFTWARE\Microsoft\Windows\CurrentVersion\Run','ScreenShoper')='') then
begin
  autostart_bool.Checked:=false;
end
else
begin
  autostart_bool.Checked:=true;
end;

if (LCP_get_registry(0,'\Software\ScreenShoper','format_img')='jpg') then
begin
format_img.ItemIndex:=0;
end
else
if (LCP_get_registry(0,'\Software\ScreenShoper','format_img')='png') then
begin
format_img.ItemIndex:=1;
end
else
if (LCP_get_registry(0,'\Software\ScreenShoper','format_img')='bmp') then
begin
format_img.ItemIndex:=2;
end
else
begin
format_img.ItemIndex:=0;
end;

autostart_tray_bool.Enabled:=autostart_bool.Checked;
autostart_tray_bool.Checked:=strtobool(LCP_get_registry(0,'\Software\ScreenShoper','autostart_tray_bool'));
close_tray_bool.Checked:=strtobool(LCP_get_registry(0,'\Software\ScreenShoper','close_tray_bool'));
copy_to_clipboard_bool.Checked:=strtobool(LCP_get_registry(0,'\Software\ScreenShoper','CopyToClipboard'));
save_to_folder_bool.Checked:=strtobool(LCP_get_registry(0,'\Software\ScreenShoper','SaveToFolder'));
folder_path.Clear;
folder_path.Text:=LCP_get_registry(0,'\Software\ScreenShoper','FolderPath');
key_save_img.HotKey:=TextToShortCut(LCP_get_registry(0,'\Software\ScreenShoper','key_save_img'));
save_folder_active;
end;

procedure TForm3.save_folder_active;
begin
folder_path.Enabled:=save_to_folder_bool.Checked;
select_folder_butt.Enabled:=save_to_folder_bool.Checked;
format_img.Enabled:=save_to_folder_bool.Checked;
end;

procedure TForm3.save_to_folder_boolClick(Sender: TObject);
begin
save_folder_active;
end;

end.
