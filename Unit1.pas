unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Imaging.pngimage, Vcl.ExtCtrls,
  Vcl.StdCtrls, System.ImageList, Vcl.ImgList, Vcl.Menus, lcp, ShellApi,
  Vcl.ComCtrls, System.Actions, Vcl.ActnList, Vcl.PlatformDefaultStyleActnCtrls,
  Vcl.ActnMan;

type
  TForm1 = class(TForm)
    img_create_screen: TImage;
    lbl_setting: TLabel;
    img_setting: TImage;
    img_create_screen_list: TImageList;
    lbl_img_top_move: TLabel;
    lbl_img_button_move: TLabel;
    img_button_list: TImageList;
    img_close: TImage;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N4: TMenuItem;
    popup_close: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    TrayIcon1: TTrayIcon;
    App_Icon: TImageList;
    PopupMenu2: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem7: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N5: TMenuItem;
    N10: TMenuItem;
    ActionList1: TActionList;
    Action1: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure img_create_screenMouseEnter(Sender: TObject);
    procedure img_create_screenMouseLeave(Sender: TObject);
    procedure img_create_screenMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure img_create_screenMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lbl_img_top_moveMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lbl_img_button_moveMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure img_closeClick(Sender: TObject);
    procedure img_settingMouseEnter(Sender: TObject);
    procedure img_settingMouseLeave(Sender: TObject);
    procedure img_settingMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure img_settingMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure img_closeMouseLeave(Sender: TObject);
    procedure img_closeMouseEnter(Sender: TObject);
    procedure img_closeMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure img_closeMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure img_create_screenClick(Sender: TObject);
    procedure img_settingClick(Sender: TObject);
    procedure popup_closeClick(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure TrayIcon1DblClick(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    procedure WMHotkey( var msg: TWMHotkey ); message WM_HOTKEY;
  public
    { Public declarations }
    var form3_show:boolean;
    function register_hot_key(keys:string):boolean;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses Unit2, Unit3;

procedure TForm1.Action1Execute(Sender: TObject);
begin
img_create_screenClick(Sender);
end;

procedure TForm1.FormCreate(Sender: TObject);
var i:integer;
Wnd:HWND;
begin
try
if (System.ParamCount>0) then
begin
  for i:= 0 to System.ParamCount do
  begin
    if trim(System.ParamStr(i))='/auto' then
      begin
        Application.ShowMainForm:=false; // ������ � ����
        TrayIcon1.Visible:=true; // �������� ������ � ����
      end;
  end;
end;

if (lcp_get_count_task_manager(ExtractFileName(application.ExeName))>1) then
begin
  lcp_task_manager_close(ExtractFileName(application.ExeName),GetCurrentProcessID());
end;

form1.Left:=screen.Width-form1.ClientWidth-25;
form1.Top:=screen.Height-form1.ClientHeight-45;
if (LCP_get_registry(0,'\Software\ScreenShoper','CopyToClipboard')='') then
begin
  LCP_set_registry(0,'\Software\ScreenShoper','CopyToClipboard','1');
end;
if (LCP_get_registry(0,'\Software\ScreenShoper','SaveToFolder')='') then
begin
  LCP_set_registry(0,'\Software\ScreenShoper','SaveToFolder','1');
end;
if (LCP_get_registry(0,'\Software\ScreenShoper','key_save_img')='') then
begin
  LCP_set_registry(0,'\Software\ScreenShoper','key_save_img','Ctrl+P');
end;
if (LCP_get_registry(0,'\Software\ScreenShoper','autostart_tray_bool')='') then
begin
  LCP_set_registry(0,'\Software\ScreenShoper','autostart_tray_bool','1');
end;
if (LCP_get_registry(0,'\Software\ScreenShoper','close_tray_bool')='') then
begin
  LCP_set_registry(0,'\Software\ScreenShoper','close_tray_bool','1');
end;
if (LCP_get_registry(0,'\Software\ScreenShoper','format_img')='') then
begin
  LCP_set_registry(0,'\Software\ScreenShoper','format_img','jpg');
end;
if (LCP_get_registry(0,'\Software\ScreenShoper','FolderPath')='') then
begin
  LCP_set_registry(0,'\Software\ScreenShoper','FolderPath',ExtractFilePath(application.ExeName)+'ScreenShoper Images');
  if (StrToBool(LCP_get_registry(0,'\Software\ScreenShoper','SaveToFolder'))=true) then
  begin
    ForceDirectories(ExtractFilePath(application.ExeName)+'ScreenShoper Images');
  end;
end;
form3_show:=false;
if (register_hot_key(LCP_get_registry(0,'\Software\ScreenShoper','key_save_img'))=false) then
begin
  i:=MessageBox(Application.Handle,pointer('�� ������� ���������������� ������� ������: '+LCP_get_registry(0,'\Software\ScreenShoper','key_save_img')+'!'+#13+'����������, �������� ��������� ������ �� �����!'),pointer(application.Title), MB_YesNo or MB_ICONError or MB_DEFBUTTON1);
  if (i=6) then
  begin
    form3_show:=true;
  end;
end;
form1.Action1.ShortCut:=TextToShortCut(LCP_get_registry(0,'\Software\ScreenShoper','key_save_img'));
except
end;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
try
UnregisterHotKey(handle,1);
except
end;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
img_create_screen_list.Draw(img_create_screen.Canvas,0,0,0);
img_button_list.Draw(img_setting.Canvas,0,0,0);
img_button_list.Draw(img_close.Canvas,0,0,3);
form2.show;
if form3_show=true then
begin
  form3_show:=false;
  form3.Show;
end;
end;

procedure TForm1.img_closeClick(Sender: TObject);
begin
if (strtobool(LCP_get_registry(0,'\Software\ScreenShoper','close_tray_bool'))=true) then
begin
  form1.Hide;
  form2.Hide;
  TrayIcon1.Visible:=true;
end
else
begin
  application.Terminate;
end;
end;

procedure TForm1.img_closeMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
img_close.Picture:= nil;
img_button_list.Draw(img_close.Canvas,0,0,4);
end;

procedure TForm1.img_closeMouseEnter(Sender: TObject);
begin
img_close.Picture:= nil;
img_button_list.Draw(img_close.Canvas,0,0,5);
end;

procedure TForm1.img_closeMouseLeave(Sender: TObject);
begin
img_close.Picture:= nil;
img_button_list.Draw(img_close.Canvas,0,0,3);
end;

procedure TForm1.img_closeMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
img_close.Picture:= nil;
img_button_list.Draw(img_close.Canvas,0,0,3);
end;

procedure TForm1.img_create_screenClick(Sender: TObject);
var filename,foldername:string;
i:integer;
begin
form1.Visible:=false;
form2.Visible:=false;
foldername:=LCP_get_registry(0,'\Software\ScreenShoper','FolderPath');

filename:=foldername+'\ScreenShoper.jpg';
i:=0;
while FileExists(filename) do
begin
  inc(i);
  filename:=foldername+'\ScreenShoper'+inttostr(i)+'.'+LCP_get_registry(0,'\Software\ScreenShoper','format_img');
end;
if (StrToBool(LCP_get_registry(0,'\Software\ScreenShoper','SaveToFolder'))=false) then
begin
  filename:='';
end
else
begin
  ForceDirectories(foldername);
end;

if (create_scrinshot(LCP_get_registry(0,'\Software\ScreenShoper','format_img'),strtobool(LCP_get_registry(0,'\Software\ScreenShoper','CopyToClipboard')),filename,form2.Shape1.Left,form2.Shape1.Top,form2.Shape1.Width,form2.Shape1.Height)=false) then
begin
form1.Visible:=true;
form2.Visible:=true;
showmessage('������ ��� ���������� ��������� � ������ *.'+LCP_get_registry(0,'\Software\ScreenShoper','format_img')+'!');
exit();
end;
form1.Visible:=true;
form2.Visible:=true;
if TrayIcon1.Visible=true then
begin
  form1.Hide;
  form2.Hide;
end;
end;

procedure TForm1.img_create_screenMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
img_create_screen.Picture:= nil;
img_create_screen_list.Draw(img_create_screen.Canvas,0,0,1);
end;

procedure TForm1.img_create_screenMouseEnter(Sender: TObject);
begin
img_create_screen.Picture:= nil;
img_create_screen_list.Draw(img_create_screen.Canvas,0,0,2);
end;

procedure TForm1.img_create_screenMouseLeave(Sender: TObject);
begin
img_create_screen.Picture:= nil;
img_create_screen_list.Draw(img_create_screen.Canvas,0,0,0);
end;

procedure TForm1.img_create_screenMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
img_create_screen.Picture:= nil;
img_create_screen_list.Draw(img_create_screen.Canvas,0,0,0);
end;

procedure TForm1.img_settingClick(Sender: TObject);
begin
PopupMenu1.Popup(form1.Left,form1.Top);
end;

procedure TForm1.img_settingMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
img_setting.Picture:= nil;
img_button_list.Draw(img_setting.Canvas,0,0,1);
end;

procedure TForm1.img_settingMouseEnter(Sender: TObject);
begin
img_setting.Picture:= nil;
img_button_list.Draw(img_setting.Canvas,0,0,2);
end;

procedure TForm1.img_settingMouseLeave(Sender: TObject);
begin
img_setting.Picture:= nil;
img_button_list.Draw(img_setting.Canvas,0,0,0);
end;

procedure TForm1.img_settingMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
img_setting.Picture:= nil;
img_button_list.Draw(img_setting.Canvas,0,0,0);
end;

procedure TForm1.lbl_img_button_moveMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
const sc_dragmove = $f012;
begin
releasecapture;
Form1.perform(wm_syscommand, sc_dragmove, 0);
end;

procedure TForm1.lbl_img_top_moveMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
const sc_dragmove = $f012;
begin
releasecapture;
Form1.perform(wm_syscommand, sc_dragmove, 0);
end;

procedure TForm1.MenuItem1Click(Sender: TObject);
begin
TrayIcon1DblClick(Sender);
end;

procedure TForm1.N1Click(Sender: TObject);
var FolderPath:string;
begin
FolderPath:=LCP_get_registry(0,'\Software\ScreenShoper','FolderPath');
ShellExecute(Application.Handle,'open', PChar(FolderPath),PChar(FolderPath), nil, SW_SHOW);
end;

procedure TForm1.N4Click(Sender: TObject);
begin
form1.Hide;
form2.Hide;
TrayIcon1.Visible:=true;
end;

procedure TForm1.N5Click(Sender: TObject);
var FolderPath:string;
begin
FolderPath:=LCP_get_registry(0,'\Software\ScreenShoper','FolderPath');
ShellExecute(Application.Handle,'open', PChar(FolderPath),PChar(FolderPath), nil, SW_SHOW);
end;

procedure TForm1.N6Click(Sender: TObject);
begin
TrayIcon1DblClick(Sender);
end;

procedure TForm1.N8Click(Sender: TObject);
begin
form1.Enabled:=false;
form2.Enabled:=false;
form3.Show;
end;

procedure TForm1.popup_closeClick(Sender: TObject);
begin
application.Terminate;
end;

function TForm1.register_hot_key(keys: string): boolean;
var
i:integer;
key_screen_str:string;
key_screen_arr:LCP_Array1;
key_screen:TShortCut;
key_mod:cardinal;
key_vk:cardinal;
begin
key_screen_str:=LowerCase(keys);
try
UnregisterHotKey(handle,1);
except
end;
try
key_screen_arr:=explode(key_screen_str,'+');
if (length(key_screen_arr)>0) then
begin
  for i:=0 to length(key_screen_arr)-1 do
  begin
    if (trim(key_screen_arr[i])='alt') then
    begin
      key_mod:=MOD_ALT;
      continue;
    end;
    if (trim(key_screen_arr[i])='ctrl') then
    begin
      key_mod:=MOD_CONTROL;
      continue;
    end;
    if (trim(key_screen_arr[i])='win') then
    begin
      key_mod:=MOD_WIN;
      continue;
    end;
    if (trim(key_screen_arr[i])='shift') then
    begin
      key_mod:=MOD_SHIFT;
      continue;
    end;
    key_vk:=TextToShortCut(key_screen_arr[i]);
  end;
end;
if not RegisterHotkey(Handle, 1, key_mod, key_vk) then
begin
result:=false;
exit();
end;
except
result:=false;
exit();
end;
result:=true;
end;

procedure TForm1.TrayIcon1DblClick(Sender: TObject);
begin
show();
setForegroundWindow(handle);
TrayIcon1.Visible:=false;
end;

procedure TForm1.WMHotkey(var msg: TWMHotkey);
begin
if msg.hotkey = 1 then
  begin
    img_create_screenClick(nil);
  end;
end;
end.
